let display = document.getElementById('box');
let lastOperationHistory = document.getElementById('last_operation_history');
let operand1 = '';
let operator = '';
let operand2 = '';

function button_number(value) {
    if (!display || !lastOperationHistory) {
        console.error("Display elements not found!");
        return;
    }

    if (value === '+' || value === '-' || value === '*' || value === '/') {
        if (operand1 !== '' && operator === '' && operand2 === '') {
            operator = value;
            display.innerText = operator;
        }
    } else if (value === '=') {
        if (operand1 !== '' && operator !== '' && operand2 === '') {
            lastOperationHistory.innerText = operand1 + ' ' + operator;
            operand2 = operand1;
        }

        if (operand1 !== '' && operator !== '' && operand2 !== '') {
            let result = calculate();
            display.innerText = result;
            lastOperationHistory.innerText = operand1 + ' ' + operator + ' ' + operand2 + ' = ' + result;
            operand1 = result;
            operator = '';
            operand2 = '';
        }
    } else {
        if (operator === '') {
            operand1 += value;
            display.innerText = operand1;
        } else {
            operand2 += value;
            display.innerText = operand2;
        }
    }
}

function calculate() {
    let num1 = parseFloat(operand1);
    let num2 = parseFloat(operand2);

    switch (operator) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            if (num2 !== 0) {
                return num1 / num2;
            } else {
                return 'Error';
            }
        default:
            return 'Error';
    }
}

function clear_entry() {
    if (operator === '') {
        operand1 = '';
    } else {
        operand2 = '';
    }
    display.innerText = 'zéro';
}

function button_clear() {
    operand1 = '';
    operator = '';
    operand2 = '';
    display.innerText = 'zéro';
    lastOperationHistory.innerText = '';
}

function backspace_remove() {
    if (operator === '') {
        operand1 = operand1.slice(0, -1);
        display.innerText = operand1 === '' ? 'zéro' : operand1;
    } else {
        operand2 = operand2.slice(0, -1);
        display.innerText = operand2 === '' ? operator : operand2;
    }
}
